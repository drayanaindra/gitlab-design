window.__imported__ = window.__imported__ || {};
window.__imported__["prototype_navigation@2x/layers.json.js"] = [
	{
		"objectId": "0D8CB85C-15FE-4E05-9B2B-72FD5704B835",
		"kind": "artboard",
		"name": "Screen_1",
		"originalName": "Screen 1",
		"maskFrame": null,
		"layerFrame": {
			"x": -523,
			"y": 150,
			"width": 1060,
			"height": 750
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "9E4DE6E4-8ABE-49FA-94BF-BADA92979ABC",
				"kind": "group",
				"name": "Content",
				"originalName": "Content",
				"maskFrame": null,
				"layerFrame": {
					"x": 7,
					"y": 0,
					"width": 1051,
					"height": 856
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-Content-ouu0reu2.png",
					"frame": {
						"x": 7,
						"y": 0,
						"width": 1051,
						"height": 856
					}
				},
				"children": [
					{
						"objectId": "C04100F9-763E-4C9D-9CB1-9F1C52AA9685",
						"kind": "group",
						"name": "Group_2_Copy",
						"originalName": "Group 2 Copy",
						"maskFrame": null,
						"layerFrame": {
							"x": 18,
							"y": 10,
							"width": 665,
							"height": 39
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group_2_Copy-qza0mtaw.png",
							"frame": {
								"x": 18,
								"y": 10,
								"width": 665,
								"height": 39
							}
						},
						"children": []
					}
				]
			}
		]
	},
	{
		"objectId": "D9307C6B-4249-40B2-B695-419EC62277AF",
		"kind": "artboard",
		"name": "Screen_2",
		"originalName": "Screen 2",
		"maskFrame": null,
		"layerFrame": {
			"x": 637,
			"y": 150,
			"width": 1060,
			"height": 750
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "0B9CAC3F-9550-40C5-B72E-08A40C3F6BA8",
				"kind": "group",
				"name": "Content1",
				"originalName": "Content",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 1237,
					"height": 858
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-Content-mei5q0fd.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 1237,
						"height": 858
					}
				},
				"children": [
					{
						"objectId": "8599BAD8-6BD1-424F-94CB-E7EF0426E3A9",
						"kind": "group",
						"name": "add_list_button",
						"originalName": "add-list-button",
						"maskFrame": null,
						"layerFrame": {
							"x": 881,
							"y": 12,
							"width": 67,
							"height": 33
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-add_list_button-odu5oujb.png",
							"frame": {
								"x": 881,
								"y": 12,
								"width": 67,
								"height": 33
							}
						},
						"children": []
					},
					{
						"objectId": "40B250C6-F079-42C1-B3F8-5F94BBDEEFD3",
						"kind": "group",
						"name": "success_secondary_button_resting",
						"originalName": "success-secondary-button--resting",
						"maskFrame": null,
						"layerFrame": {
							"x": 958,
							"y": 12,
							"width": 86,
							"height": 33
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-success_secondary_button_resting-ndbcmjuw.png",
							"frame": {
								"x": 958,
								"y": 12,
								"width": 86,
								"height": 33
							}
						},
						"children": []
					},
					{
						"objectId": "C4005641-1E19-4E32-9195-BF7AE0026938",
						"kind": "group",
						"name": "board_dropdown",
						"originalName": "board-dropdown",
						"maskFrame": null,
						"layerFrame": {
							"x": 180,
							"y": 12,
							"width": 130,
							"height": 33
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-board_dropdown-qzqwmdu2.png",
							"frame": {
								"x": 180,
								"y": 12,
								"width": 130,
								"height": 33
							}
						},
						"children": []
					},
					{
						"objectId": "906CE9A6-1511-4B7E-82D8-7B081EFEC9D1",
						"kind": "group",
						"name": "search_field",
						"originalName": "search-field",
						"maskFrame": null,
						"layerFrame": {
							"x": 320,
							"y": 12,
							"width": 551,
							"height": 33
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-search_field-ota2q0u5.png",
							"frame": {
								"x": 320,
								"y": 12,
								"width": 551,
								"height": 33
							}
						},
						"children": []
					},
					{
						"objectId": "69F002DC-FB1A-4053-81E4-3D797464050A",
						"kind": "group",
						"name": "Group_2_Copy1",
						"originalName": "Group 2 Copy",
						"maskFrame": null,
						"layerFrame": {
							"x": 17,
							"y": 21,
							"width": 152,
							"height": 17
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group_2_Copy-njlgmday.png",
							"frame": {
								"x": 17,
								"y": 21,
								"width": 152,
								"height": 17
							}
						},
						"children": []
					}
				]
			}
		]
	},
	{
		"objectId": "53F03699-942F-44AE-8A44-A27CBD9C55D8",
		"kind": "artboard",
		"name": "sidebar",
		"originalName": "sidebar",
		"maskFrame": null,
		"layerFrame": {
			"x": -2193,
			"y": 150,
			"width": 220,
			"height": 750
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "FAD3BCFD-85D9-4D21-99DB-920CEC1AFEB7",
				"kind": "text",
				"name": "Group_6",
				"originalName": "Group 6",
				"maskFrame": null,
				"layerFrame": {
					"x": 25,
					"y": 19,
					"width": 56,
					"height": 19
				},
				"visible": true,
				"metadata": {
					"opacity": 1,
					"string": "Project",
					"css": [
						"/* project: */",
						"font-family: SourceSansPro-Regular;",
						"font-size: 19px;",
						"color: #5C5C5C;",
						"letter-spacing: 0;"
					]
				},
				"image": {
					"path": "images/Layer-Group_6-rkfem0jd.png",
					"frame": {
						"x": 25,
						"y": 19,
						"width": 56,
						"height": 19
					}
				},
				"children": []
			},
			{
				"objectId": "50F662AC-ED0B-4B24-A221-AC56766B7725",
				"kind": "text",
				"name": "Group_5",
				"originalName": "Group 5",
				"maskFrame": null,
				"layerFrame": {
					"x": 25,
					"y": 59,
					"width": 85,
					"height": 18
				},
				"visible": true,
				"metadata": {
					"opacity": 1,
					"string": "Repository",
					"css": [
						"/* repository: */",
						"font-family: SourceSansPro-Regular;",
						"font-size: 19px;",
						"color: #5C5C5C;",
						"letter-spacing: 0;"
					]
				},
				"image": {
					"path": "images/Layer-Group_5-ntbgnjyy.png",
					"frame": {
						"x": 25,
						"y": 59,
						"width": 85,
						"height": 18
					}
				},
				"children": []
			},
			{
				"objectId": "2E2972F8-CF36-47B5-8EFD-431616965DE4",
				"kind": "group",
				"name": "Issues",
				"originalName": "Issues",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": 87,
					"width": 219,
					"height": 200
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "5A4FED03-639A-4744-923F-7A0396EAC6C0",
						"kind": "group",
						"name": "title",
						"originalName": "title",
						"maskFrame": null,
						"layerFrame": {
							"x": 25,
							"y": 100,
							"width": 49,
							"height": 14
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "DE381CE4-8BF7-4719-ACA4-E535F791A09A",
								"kind": "text",
								"name": "unselected",
								"originalName": "unselected",
								"maskFrame": null,
								"layerFrame": {
									"x": 25,
									"y": 100,
									"width": 49,
									"height": 14
								},
								"visible": true,
								"metadata": {
									"opacity": 1,
									"string": "Issues",
									"css": [
										"/* issues: */",
										"font-family: SourceSansPro-Semibold;",
										"font-size: 19px;",
										"color: #5C5C5C;",
										"letter-spacing: 0;"
									]
								},
								"image": {
									"path": "images/Layer-unselected-reuzodfd.png",
									"frame": {
										"x": 25,
										"y": 100,
										"width": 49,
										"height": 14
									}
								},
								"children": []
							},
							{
								"objectId": "3821B798-01D7-4EC0-A26F-0188EDFACA85",
								"kind": "group",
								"name": "selected",
								"originalName": "selected",
								"maskFrame": null,
								"layerFrame": {
									"x": 0,
									"y": 87,
									"width": 219,
									"height": 40
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-selected-mzgymui3.png",
									"frame": {
										"x": 0,
										"y": 87,
										"width": 219,
										"height": 40
									}
								},
								"children": [
									{
										"objectId": "6CDFEE4F-3921-4772-976C-294E6DDD0BEE",
										"kind": "group",
										"name": "selector",
										"originalName": "selector",
										"maskFrame": null,
										"layerFrame": {
											"x": 0,
											"y": 87,
											"width": 219,
											"height": 40
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-selector-nknerkvf.png",
											"frame": {
												"x": 0,
												"y": 87,
												"width": 219,
												"height": 40
											}
										},
										"children": []
									}
								]
							}
						]
					},
					{
						"objectId": "3CB37B20-B571-472A-87DE-FB7F032832C3",
						"kind": "group",
						"name": "sub_elements",
						"originalName": "sub_elements",
						"maskFrame": null,
						"layerFrame": {
							"x": 45,
							"y": 140,
							"width": 75,
							"height": 134
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "D6BBC83B-C405-4ABB-94E4-C8FC4B739625",
								"kind": "group",
								"name": "list",
								"originalName": "list",
								"maskFrame": null,
								"layerFrame": {
									"x": 45,
									"y": 140,
									"width": 24,
									"height": 14
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "D431A563-0F14-47C8-81E4-E47D2A9736B9",
										"kind": "group",
										"name": "unselected1",
										"originalName": "unselected",
										"maskFrame": null,
										"layerFrame": {
											"x": 45,
											"y": 140,
											"width": 24,
											"height": 14
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"children": [
											{
												"objectId": "77C26BD3-0CAE-412E-9AE7-6BDCE5EDDD0D",
												"kind": "text",
												"name": "title1",
												"originalName": "title",
												"maskFrame": null,
												"layerFrame": {
													"x": 45,
													"y": 140,
													"width": 24,
													"height": 14
												},
												"visible": true,
												"metadata": {
													"opacity": 1,
													"string": "List",
													"css": [
														"/* list: */",
														"font-family: SourceSansPro-Regular;",
														"font-size: 17px;",
														"color: #5C5C5C;",
														"letter-spacing: 0;"
													]
												},
												"image": {
													"path": "images/Layer-title-nzddmjzc.png",
													"frame": {
														"x": 45,
														"y": 140,
														"width": 24,
														"height": 14
													}
												},
												"children": []
											}
										]
									},
									{
										"objectId": "BB7D4C68-7C7B-49D3-B27B-B1FFC8AE93E5",
										"kind": "group",
										"name": "selected1",
										"originalName": "selected",
										"maskFrame": null,
										"layerFrame": {
											"x": 0,
											"y": 127,
											"width": 219,
											"height": 40
										},
										"visible": false,
										"metadata": {
											"opacity": 1
										},
										"children": [
											{
												"objectId": "B6C5E220-8A37-4865-8C52-FE94298E89D0",
												"kind": "text",
												"name": "title2",
												"originalName": "title",
												"maskFrame": null,
												"layerFrame": {
													"x": 45,
													"y": 140,
													"width": 25,
													"height": 14
												},
												"visible": true,
												"metadata": {
													"opacity": 1,
													"string": "List",
													"css": [
														"/* list: */",
														"font-family: SourceSansPro-Semibold;",
														"font-size: 17px;",
														"color: #5C5C5C;",
														"letter-spacing: 0;"
													]
												},
												"image": {
													"path": "images/Layer-title-qjzdnuuy.png",
													"frame": {
														"x": 45,
														"y": 140,
														"width": 25,
														"height": 14
													}
												},
												"children": []
											},
											{
												"objectId": "57EB6A7A-880E-40B9-8A1F-4FF5E91B39A1",
												"kind": "group",
												"name": "selector1",
												"originalName": "selector",
												"maskFrame": null,
												"layerFrame": {
													"x": 0,
													"y": 127,
													"width": 219,
													"height": 40
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-selector-ntdfqjzb.png",
													"frame": {
														"x": 0,
														"y": 127,
														"width": 219,
														"height": 40
													}
												},
												"children": []
											}
										]
									}
								]
							},
							{
								"objectId": "28691D3A-6E33-41CA-8592-B23A0C2E7C49",
								"kind": "group",
								"name": "board",
								"originalName": "board",
								"maskFrame": null,
								"layerFrame": {
									"x": 45,
									"y": 180,
									"width": 41,
									"height": 14
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "1F7128C6-C0C1-4A3F-885C-41237ABF6564",
										"kind": "group",
										"name": "unselected2",
										"originalName": "unselected",
										"maskFrame": null,
										"layerFrame": {
											"x": 45,
											"y": 180,
											"width": 41,
											"height": 14
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"children": [
											{
												"objectId": "AEA9D08F-35CD-45CB-A87B-D960BB32D61C",
												"kind": "text",
												"name": "title3",
												"originalName": "title",
												"maskFrame": null,
												"layerFrame": {
													"x": 45,
													"y": 180,
													"width": 41,
													"height": 14
												},
												"visible": true,
												"metadata": {
													"opacity": 1,
													"string": "Board",
													"css": [
														"/* list: */",
														"font-family: SourceSansPro-Regular;",
														"font-size: 17px;",
														"color: #5C5C5C;",
														"letter-spacing: 0;"
													]
												},
												"image": {
													"path": "images/Layer-title-quvbouqw.png",
													"frame": {
														"x": 45,
														"y": 180,
														"width": 41,
														"height": 14
													}
												},
												"children": []
											}
										]
									},
									{
										"objectId": "BD3D3033-52C3-4E3D-8F60-E7F6692F0A4A",
										"kind": "group",
										"name": "selected2",
										"originalName": "selected",
										"maskFrame": null,
										"layerFrame": {
											"x": 0,
											"y": 167,
											"width": 219,
											"height": 40
										},
										"visible": false,
										"metadata": {
											"opacity": 1
										},
										"children": [
											{
												"objectId": "F898CDA0-EA91-48C9-B722-0EE3AB76A67A",
												"kind": "text",
												"name": "title4",
												"originalName": "title",
												"maskFrame": null,
												"layerFrame": {
													"x": 45,
													"y": 180,
													"width": 42,
													"height": 14
												},
												"visible": true,
												"metadata": {
													"opacity": 1,
													"string": "Board",
													"css": [
														"/* list: */",
														"font-family: SourceSansPro-Semibold;",
														"font-size: 17px;",
														"color: #5C5C5C;",
														"letter-spacing: 0;"
													]
												},
												"image": {
													"path": "images/Layer-title-rjg5oene.png",
													"frame": {
														"x": 45,
														"y": 180,
														"width": 42,
														"height": 14
													}
												},
												"children": []
											},
											{
												"objectId": "9DCE8D4F-407F-463C-9376-9B141E61A7E9",
												"kind": "group",
												"name": "selector2",
												"originalName": "selector",
												"maskFrame": null,
												"layerFrame": {
													"x": 0,
													"y": 167,
													"width": 219,
													"height": 40
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-selector-ourdrthe.png",
													"frame": {
														"x": 0,
														"y": 167,
														"width": 219,
														"height": 40
													}
												},
												"children": []
											}
										]
									}
								]
							},
							{
								"objectId": "7C344660-4579-4BCA-8D6F-6E9A38623763",
								"kind": "group",
								"name": "labels",
								"originalName": "labels",
								"maskFrame": null,
								"layerFrame": {
									"x": 45,
									"y": 220,
									"width": 45,
									"height": 14
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "6B9AA17B-A045-4B6D-961D-A2DF165D2D66",
										"kind": "group",
										"name": "unselected3",
										"originalName": "unselected",
										"maskFrame": null,
										"layerFrame": {
											"x": 45,
											"y": 220,
											"width": 45,
											"height": 14
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"children": [
											{
												"objectId": "D1A73644-6F67-4209-B01A-AB74FB98173D",
												"kind": "text",
												"name": "title5",
												"originalName": "title",
												"maskFrame": null,
												"layerFrame": {
													"x": 45,
													"y": 220,
													"width": 45,
													"height": 14
												},
												"visible": true,
												"metadata": {
													"opacity": 1,
													"string": "Labels",
													"css": [
														"/* list: */",
														"font-family: SourceSansPro-Regular;",
														"font-size: 17px;",
														"color: #5C5C5C;",
														"letter-spacing: 0;"
													]
												},
												"image": {
													"path": "images/Layer-title-rdfbnzm2.png",
													"frame": {
														"x": 45,
														"y": 220,
														"width": 45,
														"height": 14
													}
												},
												"children": []
											}
										]
									},
									{
										"objectId": "0B746C08-C60B-4D9D-A1CE-D5CF57A5D1BF",
										"kind": "group",
										"name": "selected3",
										"originalName": "selected",
										"maskFrame": null,
										"layerFrame": {
											"x": 0,
											"y": 207,
											"width": 219,
											"height": 40
										},
										"visible": false,
										"metadata": {
											"opacity": 1
										},
										"children": [
											{
												"objectId": "2FBE6741-F0FF-49D5-8D73-90EDAD59867A",
												"kind": "text",
												"name": "title6",
												"originalName": "title",
												"maskFrame": null,
												"layerFrame": {
													"x": 45,
													"y": 220,
													"width": 46,
													"height": 14
												},
												"visible": true,
												"metadata": {
													"opacity": 1,
													"string": "Labels",
													"css": [
														"/* list: */",
														"font-family: SourceSansPro-Semibold;",
														"font-size: 17px;",
														"color: #5C5C5C;",
														"letter-spacing: 0;"
													]
												},
												"image": {
													"path": "images/Layer-title-mkzcrty3.png",
													"frame": {
														"x": 45,
														"y": 220,
														"width": 46,
														"height": 14
													}
												},
												"children": []
											},
											{
												"objectId": "02FEE215-BAC1-402A-96E6-EC43A0528346",
												"kind": "group",
												"name": "selector3",
												"originalName": "selector",
												"maskFrame": null,
												"layerFrame": {
													"x": 0,
													"y": 207,
													"width": 219,
													"height": 40
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-selector-mdjgruuy.png",
													"frame": {
														"x": 0,
														"y": 207,
														"width": 219,
														"height": 40
													}
												},
												"children": []
											}
										]
									}
								]
							},
							{
								"objectId": "181A52D9-B3F7-439B-A924-C536B269B038",
								"kind": "group",
								"name": "milestones",
								"originalName": "milestones",
								"maskFrame": null,
								"layerFrame": {
									"x": 45,
									"y": 260,
									"width": 75,
									"height": 14
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "0507A9E1-1C87-44A4-9D9A-44B6484C262C",
										"kind": "group",
										"name": "unselected4",
										"originalName": "unselected",
										"maskFrame": null,
										"layerFrame": {
											"x": 45,
											"y": 260,
											"width": 75,
											"height": 14
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"children": [
											{
												"objectId": "F73B6EC2-A22A-44CB-B11D-0AFCAF6BB7D4",
												"kind": "text",
												"name": "title7",
												"originalName": "title",
												"maskFrame": null,
												"layerFrame": {
													"x": 45,
													"y": 260,
													"width": 75,
													"height": 14
												},
												"visible": true,
												"metadata": {
													"opacity": 1,
													"string": "Milestones",
													"css": [
														"/* list: */",
														"font-family: SourceSansPro-Regular;",
														"font-size: 17px;",
														"color: #5C5C5C;",
														"letter-spacing: 0;"
													]
												},
												"image": {
													"path": "images/Layer-title-rjczqjzf.png",
													"frame": {
														"x": 45,
														"y": 260,
														"width": 75,
														"height": 14
													}
												},
												"children": []
											}
										]
									},
									{
										"objectId": "EE849A6D-ED76-478E-87D2-05F3CFA879F8",
										"kind": "group",
										"name": "selected4",
										"originalName": "selected",
										"maskFrame": null,
										"layerFrame": {
											"x": 0,
											"y": 247,
											"width": 219,
											"height": 40
										},
										"visible": false,
										"metadata": {
											"opacity": 1
										},
										"children": [
											{
												"objectId": "EBA10A5A-2F64-42B3-B25D-3C7573A9D801",
												"kind": "text",
												"name": "title8",
												"originalName": "title",
												"maskFrame": null,
												"layerFrame": {
													"x": 45,
													"y": 260,
													"width": 77,
													"height": 14
												},
												"visible": true,
												"metadata": {
													"opacity": 1,
													"string": "Milestones",
													"css": [
														"/* list: */",
														"font-family: SourceSansPro-Semibold;",
														"font-size: 17px;",
														"color: #5C5C5C;",
														"letter-spacing: 0;"
													]
												},
												"image": {
													"path": "images/Layer-title-rujbmtbb.png",
													"frame": {
														"x": 45,
														"y": 260,
														"width": 77,
														"height": 14
													}
												},
												"children": []
											},
											{
												"objectId": "95FC04E0-5E43-46D3-B7E4-BD7DB299A95A",
												"kind": "group",
												"name": "selector4",
												"originalName": "selector",
												"maskFrame": null,
												"layerFrame": {
													"x": 0,
													"y": 247,
													"width": 219,
													"height": 40
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-selector-otvgqza0.png",
													"frame": {
														"x": 0,
														"y": 247,
														"width": 219,
														"height": 40
													}
												},
												"children": []
											}
										]
									}
								]
							}
						]
					},
					{
						"objectId": "3167ED91-A7F7-4FF3-9F2F-7701A1699F63",
						"kind": "group",
						"name": "bg1",
						"originalName": "bg",
						"maskFrame": null,
						"layerFrame": {
							"x": 0,
							"y": 87,
							"width": 219,
							"height": 200
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-bg-mze2n0ve.png",
							"frame": {
								"x": 0,
								"y": 87,
								"width": 219,
								"height": 200
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "861AC696-3B37-4402-AC71-A958DDD79C4E",
				"kind": "text",
				"name": "Group_4",
				"originalName": "Group 4",
				"maskFrame": null,
				"layerFrame": {
					"x": 25,
					"y": 300,
					"width": 124,
					"height": 18
				},
				"visible": true,
				"metadata": {
					"opacity": 1,
					"string": "Merge Requests",
					"css": [
						"/* merge_requests: */",
						"font-family: SourceSansPro-Regular;",
						"font-size: 19px;",
						"color: #5C5C5C;",
						"letter-spacing: 0;"
					]
				},
				"image": {
					"path": "images/Layer-Group_4-odyxqum2.png",
					"frame": {
						"x": 25,
						"y": 300,
						"width": 124,
						"height": 18
					}
				},
				"children": []
			},
			{
				"objectId": "8FB51F38-AEC5-4DA7-B980-B78BBB41710F",
				"kind": "text",
				"name": "Group_3",
				"originalName": "Group 3",
				"maskFrame": null,
				"layerFrame": {
					"x": 25,
					"y": 339,
					"width": 71,
					"height": 18
				},
				"visible": true,
				"metadata": {
					"opacity": 1,
					"string": "Pipelines",
					"css": [
						"/* pipelines: */",
						"font-family: SourceSansPro-Regular;",
						"font-size: 19px;",
						"color: #5C5C5C;",
						"letter-spacing: 0;"
					]
				},
				"image": {
					"path": "images/Layer-Group_3-oezcntfg.png",
					"frame": {
						"x": 25,
						"y": 339,
						"width": 71,
						"height": 18
					}
				},
				"children": []
			},
			{
				"objectId": "AD3C9AED-3691-4F78-9D68-98024D6CBB0F",
				"kind": "text",
				"name": "Group_2",
				"originalName": "Group 2",
				"maskFrame": null,
				"layerFrame": {
					"x": 24,
					"y": 379,
					"width": 70,
					"height": 18
				},
				"visible": true,
				"metadata": {
					"opacity": 1,
					"string": "Snippets",
					"css": [
						"/* snippets: */",
						"font-family: SourceSansPro-Regular;",
						"font-size: 19px;",
						"color: #5C5C5C;",
						"letter-spacing: 0;"
					]
				},
				"image": {
					"path": "images/Layer-Group_2-quqzqzlb.png",
					"frame": {
						"x": 24,
						"y": 379,
						"width": 70,
						"height": 18
					}
				},
				"children": []
			},
			{
				"objectId": "70B85377-AD09-4F93-A9B8-F9578D9660C5",
				"kind": "text",
				"name": "Group",
				"originalName": "Group",
				"maskFrame": null,
				"layerFrame": {
					"x": 24,
					"y": 419,
					"width": 65,
					"height": 19
				},
				"visible": true,
				"metadata": {
					"opacity": 1,
					"string": "Settings",
					"css": [
						"/* settings: */",
						"font-family: SourceSansPro-Regular;",
						"font-size: 19px;",
						"color: #5C5C5C;",
						"letter-spacing: 0;"
					]
				},
				"image": {
					"path": "images/Layer-Group-nzbcoduz.png",
					"frame": {
						"x": 24,
						"y": 419,
						"width": 65,
						"height": 19
					}
				},
				"children": []
			},
			{
				"objectId": "69E30B54-96E6-4ADA-B168-3E4AD60A46CF",
				"kind": "group",
				"name": "bg",
				"originalName": "bg",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 220,
					"height": 750
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-bg-njlfmzbc.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 220,
						"height": 750
					}
				},
				"children": []
			}
		]
	},
	{
		"objectId": "17591D0B-FBF5-4A30-B101-6A56F7FFB5C6",
		"kind": "artboard",
		"name": "topbar",
		"originalName": "topbar",
		"maskFrame": null,
		"layerFrame": {
			"x": -3610,
			"y": 100,
			"width": 1280,
			"height": 50
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "1E222E4F-C1D1-43F0-A95F-F4E694FC7410",
				"kind": "group",
				"name": "sidebar_button",
				"originalName": "sidebar_button",
				"maskFrame": null,
				"layerFrame": {
					"x": 5,
					"y": 2,
					"width": 40,
					"height": 44
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-sidebar_button-muuymjjf.jpg",
					"frame": {
						"x": 5,
						"y": 2,
						"width": 40,
						"height": 44
					}
				},
				"children": []
			},
			{
				"objectId": "E7496CF0-7FB2-45FE-87AF-3597F775AD8E",
				"kind": "group",
				"name": "gitlab_logo",
				"originalName": "gitlab_logo",
				"maskFrame": null,
				"layerFrame": {
					"x": 49,
					"y": 12,
					"width": 29,
					"height": 26
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-gitlab_logo-rtc0otzd.png",
					"frame": {
						"x": 49,
						"y": 12,
						"width": 29,
						"height": 26
					}
				},
				"children": []
			},
			{
				"objectId": "FA149357-B179-43B6-A952-B05B9C975653",
				"kind": "group",
				"name": "global_breadcrumbs",
				"originalName": "global_breadcrumbs",
				"maskFrame": null,
				"layerFrame": {
					"x": 92,
					"y": 17,
					"width": 322,
					"height": 19
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-global_breadcrumbs-rkexndkz.png",
					"frame": {
						"x": 92,
						"y": 17,
						"width": 322,
						"height": 19
					}
				},
				"children": []
			},
			{
				"objectId": "509127C3-9DDB-4817-AEB1-192A8EE346F0",
				"kind": "group",
				"name": "search_bar",
				"originalName": "search_bar",
				"maskFrame": null,
				"layerFrame": {
					"x": 431,
					"y": 7,
					"width": 244,
					"height": 35
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-search_bar-nta5mti3.png",
					"frame": {
						"x": 431,
						"y": 7,
						"width": 244,
						"height": 35
					}
				},
				"children": []
			},
			{
				"objectId": "73959616-61B1-433D-B6E7-7C7BA20FA0BB",
				"kind": "group",
				"name": "plus_button",
				"originalName": "plus_button",
				"maskFrame": null,
				"layerFrame": {
					"x": 690,
					"y": 17,
					"width": 29,
					"height": 14
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-plus_button-nzm5ntk2.png",
					"frame": {
						"x": 690,
						"y": 17,
						"width": 29,
						"height": 14
					}
				},
				"children": []
			},
			{
				"objectId": "C6344E91-B5A8-49B6-981A-C4F694850857",
				"kind": "group",
				"name": "exploration_buttons",
				"originalName": "exploration_buttons",
				"maskFrame": null,
				"layerFrame": {
					"x": 735,
					"y": 18,
					"width": 289,
					"height": 17
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-exploration_buttons-qzyzndrf.png",
					"frame": {
						"x": 735,
						"y": 18,
						"width": 289,
						"height": 17
					}
				},
				"children": []
			},
			{
				"objectId": "5A27D35B-4C17-4330-A67D-02060B747C93",
				"kind": "group",
				"name": "counters",
				"originalName": "counters",
				"maskFrame": null,
				"layerFrame": {
					"x": 1075,
					"y": -2,
					"width": 145,
					"height": 53
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-counters-nueyn0qz.jpg",
					"frame": {
						"x": 1075,
						"y": -2,
						"width": 145,
						"height": 53
					}
				},
				"children": []
			},
			{
				"objectId": "31A9D7B1-0F3A-4B07-98B6-B1388EFE88E6",
				"kind": "group",
				"name": "profile",
				"originalName": "profile",
				"maskFrame": null,
				"layerFrame": {
					"x": 1227,
					"y": 11,
					"width": 40,
					"height": 26
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-profile-mzfbouq3.png",
					"frame": {
						"x": 1227,
						"y": 11,
						"width": 40,
						"height": 26
					}
				},
				"children": [
					{
						"objectId": "62099829-EB2E-4F77-A64B-459DA7A3B905",
						"kind": "group",
						"name": "user_picture",
						"originalName": "user_picture",
						"maskFrame": {
							"x": 0,
							"y": 0,
							"width": 26,
							"height": 26
						},
						"layerFrame": {
							"x": 1227,
							"y": 11,
							"width": 26,
							"height": 26
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-user_picture-njiwotk4.png",
							"frame": {
								"x": 1227,
								"y": 11,
								"width": 26,
								"height": 26
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "47C87EB3-112E-4D1D-AD81-F29E6CDF2BB1",
				"kind": "group",
				"name": "bg2",
				"originalName": "bg",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 1280,
					"height": 50
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-bg-ndddoddf.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 1280,
						"height": 50
					}
				},
				"children": []
			}
		]
	}
]